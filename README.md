# Plantilla para la elaboración de la Tesis Doctoral de la Universidad de Extremadura

He creado esta plantilla a partir de [esta](https://openwetware.org/wiki/LaTeX_template_for_PhD_thesis)
, simplificándola cuantiosamente y quedándome con lo necesario. He añadido
la portada según el formato de la UEX, pero puedes modificarla a tu gusto.
Es muy sencillo añadir más elementos entre la portada y el documento principal,
sólo fíjate en como los añado yo y lo pillarás rápido. Puedes ver el resultado
en el archivo pdf.

## Instrucciones de instalación

Simplemente copia el repositorio y ponte a editar el código. Puedes también
descargarlo como zip y descomprimirlo, o subirlo a una plataforma como
Overleaf, para editarlo allí. 

## Instrucciones de uso

He intentado que el código estuviera comentado para que fuera fácil de entender
y modificar al gusto, pero detallo algunos consejos básicos:

- La estructura del documento se divide en capítulos, cada uno en una carpeta
  separada. El fichero principal se llama `main.tex` y en él se llaman a cada
  uno de los ficheros de los capítulos. Edita los capítulos e irán apareciendo
  cuando compiles. Puedes compilar directamente desde los capítulos porque hay
  un fichero en la raíz que indica que el archivo principal es `main.tex`.
  Puedes añadir más capítulos o modificar los existentes, solo acuérdate de
  incluirlos en el principal y modificar las rutas convenientemente.

- El tamaño del documento usado es libro A4, pero basta con cambiar la opción
  `a4paper` por `b5paper` de la clase del documento (la primera instrucción del
  dichero `main.tex`) para cambiarlo a un formato libro.

- He declarado un macro (un atajo) para introducir figuras de una manera
  rápida: `\figura{fichero}{titulo}{descripción}{tamaño}{etiqueta}`. No he
  hecho algo similar para las tablas porque tienen algo más de elaboración como
  para crear un atajo con ellas. Es fácil replicar el formato del rótulo de las
  figuras, presta atención a la tabla de ejemplo. Se separa entre título y
  descripción de las figuras/tablas para no sobrecargar el índice de
  figuras/tablas.

- Para usar la nomenclatura, cada vez que quieras definir un nuevo término,
  añade el comando `\nomenclature{termino}{definicion}`. Latex se encarga de
  añadirlo a la lista de abreviaturas, junto con la página donde aparece (por
  eso hay que añadir esa instrucción justo cuando mencionas el término por
  primera vez). En Overleaf actualiza la nomenclatura automaticamente, pero en
  mi caso necesitaba utilizar el comando `makeindex main.nlo -s nomencl.ist -o
  main.nls` en una terminal en la raíz donde se encuentre el proyecto, y
  después recompilar para que se actualizara.
